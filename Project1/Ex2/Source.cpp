#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;
unordered_map<string, vector<string>> results;
vector<string> buyers;
vector<string> data;
int length;
void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

void printTable()
{
	auto iter = results.end();
	iter--;
	//	int size = results.begin()->second.size();
	int size = iter->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i == -2)
			{
				//cout << it->first << " ";
				printf("|%*s|", 13, it->first.c_str());
			}
			else if (i == -1)
				cout << "_______________";
			else
				//	cout << it->second.at(i) << " ";
				printf("|%*s|", 13, it->second.at(i).c_str());
		}
		cout << endl;
	}
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;
	if (strcmp(azCol[1], "buyer_id") == 0)
	{
		for (int i = 0; i < argc; i++)
		{
			//cout << azCol[i] << "=" << argv[i] << endl;
			if (strcmp(azCol[i], "id") == 0)
			{
				buyers.push_back(argv[i]);
			}
			else if (strcmp(azCol[i], "balance") == 0)
			{
				buyers.push_back(argv[i]);
			}
		}
			
	}
	else
	{
		for (i = 0; i < argc; i++)
		{
			//cout << azCol[i] << "=" << argv[i] << endl;
			if (strcmp(azCol[i], "id") == 0)
				data.push_back(argv[i]);
			else if (strcmp(azCol[i], "price") == 0)
				data.push_back(argv[i]);
			else if (strcmp(azCol[i], "available") == 0)
				data.push_back(argv[i]);
		}
	}
	
	return 0;
}

bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg);

bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg);
int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;
	bool ans = false;
	// connection to the database
	rc = sqlite3_open("carsDealer.db", &db);

	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}

	system("CLS");

	cout << "SELECT * from cars" << endl;

	clearTable();

	rc = sqlite3_exec(db, "SELECT * from cars", callback, 0, &zErrMsg);		//get data from cars table
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}

	cout << "SELECT * from accounts" << endl;

	clearTable();

	rc = sqlite3_exec(db, "SELECT * from accounts", callback, 0, &zErrMsg); //get data from accounts table
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	ans = carPurchase(10, 10, db, zErrMsg);//ex for not working purchase = 9,3 or 10,10 ex for working purchase = 11,17
	if (ans)
		cout << "Purchase made!" << endl;
	else
		cout << "Purchase could not be made!" << endl;

	ans = balanceTransfer(9, 3,1500, db, zErrMsg);		
	if (ans)
		cout << "transfer made!" << endl;
	else
		cout << "transfer could not be made!" << endl;
	system("PAUSE");
	sqlite3_close(db);
}
/** A function that checks if a car can be purchased by a buyer, and makes transaction if possible
input - buyer id,carid, data base ,error messages*
output - if purchase successful**/
bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	string str;
	int rc;
	int carPrice = 0;
	int buyerBalance;
	if (carid > 1)
	{
		if (data[carid * 3 - 1] == "1")		//if car available
		{
			cout << "Nice" << endl;
			carPrice = stoi(data[carid * 3 - 2]);
			buyerBalance = stoi(buyers[buyerid*2 - 1]);		//get prices
			cout << "CarPrice = " << carPrice << " , BuyerBalance = " << buyerBalance << endl;
			if (buyerBalance >= carPrice)		//if buyer can pay
			{
				clearTable();
				str = "update cars set available = \"0\" where id = " + to_string(carid);	//change car availability
				rc = sqlite3_exec(db, str.c_str(), NULL, 0, &zErrMsg);
				if (rc != SQLITE_OK)
				{
					cout << "SQL error: " << zErrMsg << endl;
					sqlite3_free(zErrMsg);
					system("Pause");
					return 0;
				}
				clearTable();
				cout << "ez" << endl;
				str = "update accounts set balance = " + to_string(buyerBalance - carPrice) + " where id = " + to_string(buyerid);
				rc = sqlite3_exec(db, str.c_str(), NULL, 0, &zErrMsg);		//make money transaction
				if (rc != SQLITE_OK)
				{
					cout << "SQL error: " << zErrMsg << endl;
					sqlite3_free(zErrMsg);
					system("Pause");
					return 0;
				}
				return true;
			}
			else
				return false;
		}
		else
			return false;
	}
	else
	{
		return false;
	}
}

/** A function that does transactions
input - giver,taker,how much money,data base,error message
output - if transaction successful**/
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	string str;
	int rc;
	int fBalance;
	int tBalance;
	if (from > 1)
		fBalance = stoi(buyers[from * 2 - 1]);		//get balance of giver
	else
		fBalance = stoi(buyers[1]);

	if (to > 1)					
		tBalance = stoi(buyers[from * 2 - 1]);			//get balance of taker
	else
		tBalance = stoi(buyers[1]);

	if (amount > fBalance)		//if giver can actualy give
		return false;
	rc = sqlite3_exec(db, "begin transaction", NULL, 0, &zErrMsg);		//start transaction
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 0;
	}

	clearTable();
	
	str = "update accounts set balance = " + to_string(fBalance - amount)+ " where id = " + to_string(from);
	
	rc = sqlite3_exec(db, str.c_str(), NULL, 0, &zErrMsg);		//take money from giver
	
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 0;
	}
	
	clearTable();
	
	str = "update accounts set balance = " + to_string(tBalance + amount);
	str += " where id = " + to_string(to);	
	rc = sqlite3_exec(db, str.c_str(), NULL, 0, &zErrMsg);		//give money to taker
	
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 0;
	}

	rc = sqlite3_exec(db, "commit", NULL, 0, &zErrMsg);		//finish transaction

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 0;
	}
	return true;
}